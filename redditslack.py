#!/usr/bin/python

import praw, warnings, subprocess
import os, time, re, urllib2
from slackclient import SlackClient
from math import sqrt
warnings.filterwarnings('ignore')

# Connection to slack
token = "xoxp-9999999" #Insert correct token here

sc = SlackClient(token)

# Create user agent for Reddit api
user_agent = ("BestOf-Slack Bot 0.1")

# Create connection to reddit
r = praw.Reddit(user_agent = user_agent)

"Post the MW word of the day to a channel"
def postMW(channel):
    url = "https://www.merriam-webster.com/word-of-the-day"
    output = urllib2.urlopen(url).read()
    a = re.search(r'<title>(.+?) \| Merri', output)
    texti = "*%s*\n" %a.group(1)
    a = re.search(r'<h2>Definition</h2>(.+?)</div>', output, flags=re.DOTALL).group(1)
    texti += "Definition"
    a = re.sub(r'<.+?>', '', a)
    a = re.sub(r' +', ' ', a)
    a = re.sub(r'\n ', '\n', a)
    texti += a
    texti += "\nLink: https://www.merriam-webster.com/word-of-the-day"
    texti.replace('&', '&amp;')
    texti.replace('<', '&lt;')
    texti.replace('>', '&gt;')
    sc.api_call("chat.postMessage", username="WordOfTheDayBot", channel=channel, text=texti)


times = ["day","week","month","year","all"]

"""
Post top submissions during last "day","week","month","year","all" (time)
of subreddit or list of subreddits (srname) 
to slack channel (channel)
dry=True means nothing is posted, just printed to terminal (for debugging)
commando=True means ignore what has already been posted, showing pure top list; 
    standard behaviour is to not re-post submissions already showed.
"""
def postit(srname, channel, time='day', maxposts=5, dry=False, commando=False):
    if len(srname) < 1: return
    '''
    try:
        idfile = open('posted.txt','r')
        ids = idfile.readlines()
        idfile.close()
    except:
        ids = []
    #just cropping empty ids.
    srs = []
    for sr in ids:
        if len(sr) > 1:
            srs.append(sr.strip())
    ids = srs'''
    ids = []
    alltext = ''
    if isinstance(srname, basestring):
        subreddit = r.subreddit(srname)
        #for time in times:
        #    sss = subreddit.top(time, limit=5)
        #    if len(list(sss)) == 5: break
        if time =='all': wtime = 'All time'
        else: wtime = 'last ' + time
        toptext = "Top %i submissions to %s of %s:" % (maxposts, srname, wtime)
        # Loop through the new submissions. We're only grabbing 5 each time
        sss = list(subreddit.top(time, limit=maxposts))
    else:
        time = 'day'
        sss = []
        for sr in srname:
            try:
                subreddit = r.subreddit(sr)
                aya = list(subreddit.top(time, limit=maxposts))
                scorefac = 100 #Weighing top results from each sub higher.
                for a in aya:
                    a.score *= scorefac #Such that each sub is represented.
                    scorefac = sqrt(scorefac)
                sss += aya
            except:
                continue
        toptext = "Top %i submissions to (%s) of last %s:\n" % (maxposts, ', '.join(srname), time)
        sss.sort(key=lambda x: -x.score)
        #if len(sss) > 5:
        #    sss = sss[:5]
    if len(sss) < 1: return
    if dry:
        print toptext
    else:
        alltext += toptext
        #sc.api_call("chat.postMessage", username="RedditTopBot", channel=channel, text=toptext)
    i = 0
    for submission in sss:
        if i >= maxposts: break
        #print submission.title
        #print submission.url
        idd = submission.url #submission.id
        if (idd in ids) and not commando:
            print 'already posted', submission.title
            continue
        ids.append(idd)
        i += 1
        
        texti = submission.title + ' - ' + submission.url + ' - Comments ' + submission.shortlink + '\n'
        if dry:
            print texti
        else:
            alltext += texti
    sc.api_call("chat.postMessage", username="RedditTopBot", channel=channel, text=alltext)
    ''' #I used these lines for saving urls to avoid reposts, but unicode not supported.
    if not (dry or commando):
        idfile = open('posted.txt','w')
        for line in ids:      
            if len(line) < 1: continue  
            idfile.write(line.encode('utf-8'))
            idfile.write('\n')
        idfile.close()'''

'''
List all subreddits associated to channel
'''
def listall(channel, ret = False):
    fnm = channel + '.txt'
    try:
        idfile = open(fnm,'r')
        ids = idfile.readlines()
        idfile.close()
    except:
        ids = []
    ids = [idd.strip() for idd in ids]
    if len(ids) == 0:
        texti = "No subreddits associated to this channel :("
    else:
        texti = "Subreddits associated to the current channel 24 hour feed:\n"
        texti += ", ".join(ids)
    if not ret:
        sc.api_call("chat.postMessage", username="RedditTopBot", channel=channel, text=texti)
    else: 
        return ids

'''
Add a subreddit to a channel.
If the file "<channelID>.txt" does not exist, it is created.
Then the subreddit name is added to the file.
'''
def add(channel, sr):
    fnm = channel + '.txt'
    try:
        idfile = open(fnm,'r')
        ids = idfile.readlines()
        idfile.close()
    except:
        ids = []
    try:
        ids.append(sr)
        idfile = open(fnm,'w')
        idfile.write('\n'.join(ids))
        idfile.close() 
    except:
        return False 
    return True  

'''Removes a subreddit from a given channel'''
def remove(channel, sr):
    fnm = channel + '.txt'
    try:
        idfile = open(fnm,'r')
        ids = idfile.readlines()
        idfile.close()
    except:
        ids = []
    try:
        ids.remove(sr)
        idfile = open(fnm,'w')
        idfile.write('\n'.join(ids))
        idfile.close() 
    except:
        return False 
    return True  

i = int((time.time()/3600 + 12)/24) #-1 #-1 here to always post when rebooted lol
    
maxi = 5
print sc.rtm_connect()

"""
The actual infinite loop for listening
"""

sc.api_call("chat.postMessage", username="RedditBotEtAl", channel="C23098VL0", text="Rebooted Sucessfully. Beep Boop.")

while True:
    events =  sc.rtm_read()
    for e in events:
        try:
            if e[u'username'] == 'RedditTopBot': continue #ignore own posts to avoid feedback
        except:
            pass
        try: 
            tajp = e[u'type']
        except:
            continue
        if tajp == u'message':
            print e #Prints the message that activated the bot to stdout, mostly for debugging.
            try:
                txt = e[u'text'].lower() #Extract message text
                assert(len(txt) > 1)
            except: #No message text?
                try: #Might be a Giphy, then the actual message is the title of the attachment.
                    txt = e[u'attachments'][0][u'title'].lower()
                    assert(len(txt) > 1)
                except:
                    print "Message, but no title or text?"
                    continue
            channel = e[u'channel'] #Current channel (where activating message was posted)
            try:
                print txt #unicode problems might arise or something
            except:
                continue
            if ('top' in txt or 'best' in txt) and ('reddit' in txt): #post top submissions on demand
                for timez in times:  #obfuscated code to find if/which time period to post.
                    if ' ' + timez in txt:
                        timez = timez
                        break
                else: #no break
                    timez = 'all'
                    
                rname =  re.search(r'"(\w+)"', e[u'text']) #just grab whatever is between quotes
                nposts =  re.search(r'(\d+)', e[u'text']) #any integer gives number of posts
                if nposts:
                    nposts = int(nposts.group(1)) #... could be very large lol
                else:
                    nposts = 5
                try:
                    if rname:
                        sr = rname.group(1)
                        postit(sr, channel, time=timez, maxposts=nposts, dry=False, commando=True)
                    else:
                        sc.api_call("chat.postMessage", username="RedditTopBot", channel=channel, text='Please enclose the subreddit name in "quotes".')
                except:
                    sc.api_call("chat.postMessage", username="RedditTopBot", channel=channel, text='Some error occured... Trying hard not to kill myself.')
            elif ('redditbot' in txt):  #all possible controls for reddit 24 h feed
                if 'list' in txt:
                    listall(channel)
                    texti = ''
                elif 'add' in txt:
                    sr = txt.split('add')[1].strip()
                    if len(sr.split()) > 1:
                        texti = 'Plz just add one subreddit at a time'
                    else:
                        doe = add(channel, sr)
                        if doe:
                            texti = 'OK'
                        else:
                            texti = 'Something went wrong :('
                elif 'remove' in txt:
                    sr = txt.split('remove')[1].strip()
                    if len(sr.split()) > 1:
                        texti = 'Plz just remove one subreddit at a time'
                    else:
                        doe = remove(channel, sr)
                        if doe:
                            texti = 'OK'
                        else:
                            texti = 'Something went wrong :('
                elif ('nlp' in txt or 'natural language' in txt) and ('hard' in txt or 'difficult' in txt) and ('?' in txt):
                    texti = 'Yes, Natural language is hard!'
                elif 'reset timer' in txt:
                    i -=1 
                    texti = 'Autoposting now... reposting evert midday GMT'
                elif 'time' in txt:
                    hrs = 86400 - i
                    hrs, mins = hrs/3600, hrs%3600
                    mins, secs = mins/60, mins%60
                    texti = '%02ih, %02im, %02is until next autopost.' % (hrs, mins, secs)
                elif 'max' in txt:
                    try:
                        inti = int(txt.split('max')[1])
                        if inti < 0 or inti > 20:
                            texti = "I can't allow you to do that."
                        else:
                            maxi = inti
                            texti = 'OK. New max posts per day in each channel is %i.' % maxi
                    except:
                        texti = "Was not able to set new max. give an int plx"
                else:
                    texti = "I don't understand that request. Until now I understand:\n"
                    texti += '''`add <subreddit>` add a sub to this channel 24h feed,
`remove <subreddit>` remove sub,
`list` list all subreddits in this channel 24 h feed,
`top "<subreddit>" [day, week, month, year, all]` Post top submissions here and now,
`reset timer` 24h until next feed,
`time` show time until next feed,
`max <1-20>` Set the max amount of posts for the 24 h feed. Affects _all_ channels.'''
                if len(texti) > 1:
                    #texti has been set to the (possibly error-) message to post to Slack.
                    sc.api_call("chat.postMessage", username="RedditTopBot", channel=channel, text=texti)

            #Stuff for T-bane bot here    
            elif ('bane' in txt) and ('?' in txt) and len(txt) < 30:            
                call = './subway.sh'
                try:
                    frafra = re.sub(r'\W+', '', txt.split('fra')[1].strip())
                except:
                    frafra = 'forskningsparken'
                if frafra in ['blindern', 'universitetet', 'campus']:
                    stopp = 'Blindern'
                elif frafra in ['forskningsparken', 'vestgrensa', 'domus', 'porskningsfarken']:
                    stopp = 'Forskningsparken'
                elif frafra in ['grorud', 'robert']:
                    stopp = 'Grorud'
                elif frafra in ['stortinget', 'centrum']:
                    stopp = 'Stortinget'
                elif frafra in ['jernbanetorget', 'oslos', 'jernbarnetoget']:
                    stopp = 'Jernbanetorget'
                elif frafra in ['storo', 'daniel']:
                    stopp = 'Storo'
                elif 'carl' in frafra or 'cb' in frafra:
                    stopp = 'CB'
                elif 'lamb' in frafra or 'roar' in frafra:
                    stopp = 'Lambertseter'
                elif 'dtvet' in frafra or 'silje' in frafra or 'eigil' in frafra:
                    stopp = 'Rodtvet'
                elif 'sogns' in frafra or 'krings' in frafra:
                    stopp = 'Sognsvann'
                else:
                    texti = 'Kjenner ikke det stoppet! Jeg vet bare om:\n'
                    texti += 'Blindern, Grorud, Stortinget, Jernbanetorget, Storo, Carl Berner, Sognsvann, Forskningsparken, Lambertseter, Rodtvet'
                    sc.api_call("chat.postMessage", username="T-Bane Bot", channel=channel, text=texti)
                    continue
                proc = subprocess.Popen([call,stopp],stdout=subprocess.PIPE)
                texti = "*Neste baner fra %s:*\n%s" % (stopp, proc.stdout.read())
                sc.api_call("chat.postMessage", username="T-Bane Bot", channel=channel, text=texti)
                
                
            else: #Daniel reply bot here
                txt = re.sub(r'\W+', '', txt) #remove all non-word characters
                if txt in ['hvaskjer', 'skjer']: #if text solely consists of "[hva]skjer", post passive aggressive reply.
                    texti = "Scroll up, you noob!"
                    sc.api_call("chat.postMessage", username="DanielBot", channel=channel, text=texti)
                
                
                
    #wait one second and tick counter            
    time.sleep(1)
    #i += 1
    dayz = int((time.time()/3600 + 12) /24)
    if dayz != i: #autopost each 24 h
        print(i, dayz)
        i = dayz
        for channel in ['C39AB6C94', 'C23098VL0', 'G2QL77B0X']: #the three channels are hardcoded..
            ids = listall(channel, ret=True)
            srs = []
            for sr in ids:
                if len(sr) > 1:
                    srs.append(sr)
            postit(srs, channel, maxposts=maxi)
        #Post word of the day to swag
        channel = 'C23098VL0'
        postMW(channel)
