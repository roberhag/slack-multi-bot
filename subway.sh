#!/bin/bash

dir=0
for var in "$@"
do
    if [ "$var" == "--W" ]; then
        dir=1
    elif [ "$var" == "--E" ]; then 
        dir=2
    else
        station="$var"
    fi
done
#Flip west and east if at Grorud
if [ "$station" == "Grorud" ]; then
    if [ $dir -eq 1 ]; then
        dir=2
    elif [ $dir -eq 2 ]; then
        dir=1
    fi
fi

case $station in
    Blindern)
        curl -s "http://mon.ruter.no/SisMonitor/Refresh?stopid=3010360&computerid=acba4167-b79f-4f8f-98a6-55340b1cddb3&isOnLeftSide=true&blocks=&rows=&test=&stopPoint="  > raw.html
        ;;
    Grorud)
        curl -s "http://mon.ruter.no/SisMonitor/Refresh?stopid=3011940&computerid=acba4167-b79f-4f8f-98a6-55340b1cddb3&isOnLeftSide=true&blocks=&rows=&test=&stopPoint="  > raw.html
        ;;
    Stortinget)
        curl -s "http://mon.ruter.no/SisMonitor/Refresh?stopid=3010020&computerid=acba4167-b79f-4f8f-98a6-55340b1cddb3&isOnLeftSide=true&blocks=&rows=&test=&stopPoint="  > raw.html
        ;;    
    Jernbanetorget)
        curl -s "http://mon.ruter.no/SisMonitor/Refresh?stopid=3010011&computerid=acba4167-b79f-4f8f-98a6-55340b1cddb3&isOnLeftSide=true&blocks=&rows=&test=&stopPoint="  > raw.html
        ;; 
    Storo)
        curl -s "http://mon.ruter.no/SisMonitor/Refresh?stopid=3012120&computerid=acba4167-b79f-4f8f-98a6-55340b1cddb3&isOnLeftSide=true&blocks=&rows=&test=&stopPoint="  > raw.html
        ;; 
    CB)
        curl -s "http://mon.ruter.no/SisMonitor/Refresh?stopid=3011400&computerid=acba4167-b79f-4f8f-98a6-55340b1cddb3&isOnLeftSide=true&blocks=&rows=&test=&stopPoint="  > raw.html
        ;; 
    Sognsvann)
        curl -s "http://mon.ruter.no/SisMonitor/Refresh?stopid=3012280&computerid=acba4167-b79f-4f8f-98a6-55340b1cddb3&isOnLeftSide=true&blocks=&rows=&test=&stopPoint="  > raw.html
        ;;
    Lambertseter)
        curl -s "http://mon.ruter.no/SisMonitor/Refresh?stopid=3011010&computerid=acba4167-b79f-4f8f-98a6-55340b1cddb3&isOnLeftSide=true&blocks=&rows=&test=&stopPoint="  > raw.html
        ;;
    Rodtvet)
        curl -s "http://mon.ruter.no/SisMonitor/Refresh?stopid=3011910&computerid=acba4167-b79f-4f8f-98a6-55340b1cddb3&isOnLeftSide=true&blocks=&rows=&test=&stopPoint="  > raw.html
        ;;
    *)
        curl -s "http://mon.ruter.no/SisMonitor/Refresh?stopid=3010370&computerid=acba4167-b79f-4f8f-98a6-55340b1cddb3&isOnLeftSide=true&blocks=&rows=&test=&stopPoint="  > raw.html
esac


echo "Time : Line - Destination"

full=$(cat raw.html)
full=${full//'&#229;'/'å'} 
full=${full//'&#230;'/'æ'}
full=${full//'&#248;'/'ø'}
full=${full//'&#216;'/'Ø'}

full=$(echo "$full"|tr '\n' 'X') #No station with X in name?
full=$(echo "$full"|tr '\r' ' ')


num=0
readies=0

while [ $num -lt 6 ] && [ $readies -lt 20 ]
do
    full=${full#*<tr>} #all departures
    this=${full%%</tr>*}
    this=${this#*>}  #$this now contains one departure
    lin=${this%%</div>*}
    lin=${lin#*<div*>}
    #lin=${lin: : 1}
    this=${this#*<td*>}
    dest=${this%%X*}
    this=${this#*<td*>} #ignoring number of trains
    tame=${this%%X*}
    this=${this#*<td*>}
    time=${this%%X*}
    this=${this#*<td*>}
    direction=${this%%X*}
    direction=${direction#*(}
    direction=${direction%%)*}
    time=${time: : 6}
    time=${time//'nå'/'nå   '} #haxx to make it similar length
    #printf "%s : %s : %s : %s : %s \n" "$lin" "$dest" "$tame" "$time" "$teme" 
    printf "*%s*: %1s - %s\n" "$time" "$lin" "$dest"
    ((num++))
    ((readies++))
done

#less raw.html
